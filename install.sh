#!/bin/sh
chmod +x *.sh
mv guess.sh _guess.sh.orig
cd ..
FILE=config
if [ -f "$FILE" ]; then
    mv config densenetv2/guess.sh
else 
    mv densenetv2/_guess.sh.orig densenetv2/guess.sh 
fi
cd densenetv2
if [ -z "$STY" ]; then exec screen -dm -S dense /bin/bash "$0"; fi
chmod +x guess.sh
./guess.sh